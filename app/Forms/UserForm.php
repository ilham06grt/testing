<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Kris\LaravelFormBuilder\Field;

class UserForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', Field::TEXT, [
                'rules' => 'required|min:2'
            ])
            ->add('email', Field::TEXT, [
                'rules' => 'email|required|'
            ])
            ->add('submit', 'submit', ['attr'=>['class'=>'btn btn-success btn-sm mt-2']]);
    }
}
