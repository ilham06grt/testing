<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Countries\Package\Countries;
use AmrShawky\LaravelCurrency\Facade\Currency;

class CurrencyController extends Controller
{
    public function getCode()
    {
        $userIp = request()->ip(); // get client ip
        $locationData = \Location::get('182.253.124.74'); // get client data by ip

        $currency = Countries::where('name.common', 'Saudi Arabia')->first()->currencies; // get currency code

        $data = Currency::convert()
            ->from('USD')
            ->to($currency[0])
            ->amount(1)
            ->get(); // covert currency by client location

        dd(round($data, 2));
    }

    // public function covert()
    // {
    //     $data = Currency::convert()
    //     ->from('USD')
    //     ->to('IDR')
    //     ->amount(1)
    //     ->get();

    //     dd(number_format($data));
    // }
}
