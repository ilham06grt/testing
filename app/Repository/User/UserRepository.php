<?php 

namespace App\Repository\User;

use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
   public function getAllUsers() 
    {
        return User::latest()->paginate(10);
    }

    public function getUserById($userId) 
    {
        return User::findOrFail($userId);
    }

    public function deleteUser($userId) 
    {
        User::destroy($userId);
    }

    public function createUser(array $userDetails) 
    {
        return User::create($userDetails);
    }

    public function updateUser($userId, array $newUserDetails) 
    {
        return User::whereId($userId)->update($newUserDetails);
    }
}
