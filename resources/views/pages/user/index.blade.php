@extends('layouts.master')

@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <a href="{{ route('user.create') }}" class="btn btn-sm btn-success">Create Data</a>
                <div class="card mt-2 mb-3">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                                @forelse ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
